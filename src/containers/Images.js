import React, { Component } from 'react'
import { addImages } from '../actions';
import { connect } from 'react-redux'

class Images extends Component {
    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/photos')
        .then(res => res.json())
        .then(imgs => { this.props.save(imgs) })
    }

    render() {
        return (
            <div>
                { this.props.images.map(({url, title}) => <img src={url} alt={title} />) }
            </div>
        )
    }
}

const mapStateToProps = state => state
const mapDispatchToProps = dispatch => ({
    save: images => dispatch(addImages(images))
})

export default connect(mapStateToProps, mapDispatchToProps)(Images)
