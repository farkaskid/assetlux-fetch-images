export const ADD_IMAGES = Symbol('Add images')

export const addImages = images => ({ type: ADD_IMAGES, images })
