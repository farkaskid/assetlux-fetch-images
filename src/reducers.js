import { ADD_IMAGES } from "./actions";
import { sample } from "./utils";

const initialState = {
    images: []
}

const fetchImages = (state = initialState, action) => {
    switch (action.type) {
        case ADD_IMAGES:
            const images = []

            for (let i = 0; i < 10; i++) {
                images.push(sample(action.images))
            }

            return {...state, ...{images}}

        default:
            return state
    }
}

export default fetchImages