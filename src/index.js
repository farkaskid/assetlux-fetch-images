import React from 'react';
import App from './components/App';
import rootReducer from './reducers';
import registerServiceWorker from './registerServiceWorker';

import { render } from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux';

const store = createStore(rootReducer)

render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);
    
registerServiceWorker();
